import 'dart:io';

void main(List<String> arguments) {
  print("Menu");
  selectMenu();
  String? menu = stdin.readLineSync();

  while (menu != "5") {
    if (menu == "1") {
      print("Enter the value for x : ");
      int x = int.parse(stdin.readLineSync()!);
      print("Enter the value for y : ");
      int y = int.parse(stdin.readLineSync()!);
      print("Sum of the two numbers is : ");
      print(mathAdd(x, y));
    }
    if (menu == "2") {
      print("Enter the value for x : ");
      int x = int.parse(stdin.readLineSync()!);
      print("Enter the value for y : ");
      int y = int.parse(stdin.readLineSync()!);
      print("Subtract of the two numbers is : ");
      print(mathSubtract(x, y));
    }
    if (menu == "3") {
      print("Enter the value for x : ");
      int x = int.parse(stdin.readLineSync()!);
      print("Enter the value for y : ");
      int y = int.parse(stdin.readLineSync()!);
      print("Multiplication of the two numbers is : ");
      print(mathMulti(x, y));
    }
    if (menu == "4") {
      print("Enter the value for x : ");
      double x = double.parse(stdin.readLineSync()!);
      print("Enter the value for y : ");
      double y = double.parse(stdin.readLineSync()!);
      print("Division of the two numbers is : ");
      print(mathDivide(x, y));
    }
    selectMenu();
    menu = stdin.readLineSync();
  }
}

void selectMenu() {
  print("Select the choice you want to perform : ");
  print("1.ADD");
  print("2.SUBTRACT");
  print("3.MULTIPLY");
  print("4.DIVIDE");
  print("5.EXIT");
}

double mathDivide(double x, double y) {
  double z = double.parse((x / y).toStringAsFixed(2));
  return z;
}

int mathMulti(int x, int y) {
  return x * y;
}

int mathSubtract(int x, int y) {
  return x - y;
}

int mathAdd(int x, int y) {
  return x + y;
}
