import 'dart:io';

void main(List<String> arguments) {
  print("Please input paragraph that you want to count a words");
  String? para = stdin.readLineSync().toString().toLowerCase();

  var count = para.split(" ");
  print("The number of words from the paragraph is: ");
  print(count.length);

  for (int i = 0; i < count.length; i++) {
    var countTemp = 1;
    for (int j = i + 1; j < count.length; j++) {
      if (count[i] == count[j]) {
        countTemp++;
        count.removeAt(j);
      }
    }
    print(count[i] + ": " + countTemp.toString() + " word");
  }
  print("↓↓ After remove duplicate words in list ↓↓");
  print(count);
}
